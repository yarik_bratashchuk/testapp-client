const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config.development');
const t = require('./src/config').baseUrl;

const port = 3000;

/* eslint-disable no-console, no-unused-vars, consistent-return */
new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  proxy: {
    '/api': {
      target: t
    },
    '/oauth': {
      target: t
    }
  }
}).listen(port, 'localhost', (err, result) => {
  if (err) {
    return console.log(err);
  }
  console.log('Listening');
});
