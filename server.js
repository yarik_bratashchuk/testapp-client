const express = require('express');
const path = require('path');
const httpProxy = require('http-proxy');

const apiProxy = httpProxy.createProxyServer();

const app = express();
const port = process.env.PORT || 3000;
const t = require('./src/config').baseUrl;

app.all('/api/*', (req, res) => apiProxy.web(req, res, { target: t }));
app.all('/oauth/*', (req, res) => apiProxy.web(req, res, { target: t }));

app.use(express.static(path.join(__dirname, '/dist')));
app.route('/*').get((req, res) =>
  res.sendFile(path.join(__dirname, '/dist/index.html'))
);

const server = require('http').createServer(app);

server.listen(port, () => console.log(`TestApp listening on port ${port}`));
