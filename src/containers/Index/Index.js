import React, { PropTypes } from 'react';
// import ImmutablePropTypes from 'react-immutable-proptypes';
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';

// import {
// } from '../../utils/selectors';


const propTypes = {
};

const IndexContainer = () => (
  <div className="indexPage">
    <img src="https://test-app-by-yarik.s3-us-west-1.amazonaws.com/images/material-ui-logo.svg" alt="material-ui" />
    <h1>Welcome here!</h1>
    <h2>
      This is a test app build on React&Redux
      API server is built on Go
    </h2>
  </div>
);

IndexContainer.propTypes = propTypes;

// function mapStateToProps() {
//   return {};
// }


// function mapDispatchToProps(dispatch) {
//   return bindActionCreators({
//   }, dispatch);
// }

// export default connect(mapStateToProps, mapDispatchToProps)(IndexContainer);

export default IndexContainer;
