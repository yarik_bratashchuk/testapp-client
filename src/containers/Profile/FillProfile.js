import React, { Component, PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form';
import FlatButton from 'material-ui/FlatButton';
import Autocomplete from 'react-google-autocomplete';
import { push } from 'react-router-redux';

import { createError } from '../../actions/ui';
import { urls } from '../../routes';


/* eslint-disable react/prop-types */
const renderField = ({ input, label, type, name, meta: { touched, error } }) => (
  <div className={`form-group ${(!touched || error === undefined) ? '' : 'has-error'}`}>
    {touched && error
      ? <label htmlFor={`${name}-id`} className="error">{error}</label>
      : <label htmlFor={`${name}-id`}>{label}</label>
    }
    { name === 'address' ?
      <Autocomplete
        id={`${name}-id`}
        className="form-google"
        type={type}
        placeholder={label}
        {...input}
      /> :
      <input
        id={`${name}-id`}
        className="form-control"
        placeholder={label}
        type={type}
        {...input}
      />
    }
  </div>
);
/* eslint-enable react/prop-types */


const propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired
};


class FillProfile extends Component {
  /* eslint-disable no-unused-vars */
  static handleSubmit(value, dispatch) {
  /* eslint-enable no-unused-vars */
    fetch('/api/v1/my/profile',
      { method: 'POST',
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${window.localStorage.getItem('jwt_testapp')}`,
          'Content-Type': 'application/json'
        },
        // in the real life we should encrypt email and password
        // but this is just a sandbox app
        body: JSON.stringify(value)
      })
      .then(response =>
        (response.json())
      )
      .then((json) => {
        if (json.status === 'success') {
          dispatch(push(urls.profile));
          return;
        }
        dispatch(createError(json.error));
      });
  }

  render() {
    const { handleSubmit, pristine, submitting, reset } = this.props;

    return (
      <div className="signupForm">
        <div className="col-md-3" />
        <h1>Finish signup</h1>
        <div className="col-md-6 col-offset-3">
          <form onSubmit={handleSubmit(FillProfile.handleSubmit)}>
            <Field
              name="fullName"
              type="text"
              label="Full name"
              component={renderField}
              placeholder="Full name"
            />
            <Field
              name="address"
              type="text"
              label="Address"
              component={renderField}
              placeholder="Address"
            />
            <Field
              name="telephone"
              type="telephone"
              label="Telephone"
              component={renderField}
              placeholder="telephone"
            />
            <div className="buttons">
              <FlatButton type="submit" label="Submit" disabled={submitting} />
              <FlatButton
                type="button"
                label="Clear values"
                disabled={pristine || submitting}
                onClick={reset}
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

FillProfile.propTypes = propTypes;

export default reduxForm({
  form: 'fillProfileForm'
})(FillProfile);
