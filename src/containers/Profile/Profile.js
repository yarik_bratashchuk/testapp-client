import React, { Component, PropTypes } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as ProfileActions from '../../actions/profile';
import ProfileForm from '../../components/Profile/ProfileForm';


const propTypes = {
  loadProfile: PropTypes.func.isRequired,
  user: ImmutablePropTypes.map.isRequired,
  isLoading: PropTypes.bool.isRequired,
  updateProfile: PropTypes.func.isRequired,
  updateProfileField: PropTypes.func.isRequired
};


class Profile extends Component {
  componentWillMount() {
    this.props.loadProfile();
  }

  render() {
    const { user, isLoading, updateProfile, updateProfileField } = this.props;
    return (
      <ProfileForm
        user={user}
        isLoading={isLoading}
        updateProfile={updateProfile}
        updateField={updateProfileField}
      />
    );
  }
}

Profile.propTypes = propTypes;

function mapStateToProps({ profile }) {
  return {
    user: profile.get('userData'),
    isLoading: profile.isLoading
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ProfileActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
