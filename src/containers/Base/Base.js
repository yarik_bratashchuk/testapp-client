import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { lifecycle } from 'recompose';
import { connect } from 'react-redux';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import Loading from '../../components/Loading/Loading';
import NavButton from '../../components/NavButton/NavButton';

import * as ProfileActions from '../../actions/profile';
import * as UiActions from '../../actions/ui';

const propTypes = {
  children: PropTypes.element,
  isLoading: PropTypes.bool.isRequired,
  validateToken: PropTypes.func.isRequired,
  authorized: PropTypes.bool,
  moveToSignup: PropTypes.func.isRequired,
  moveToLogin: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
  createError: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  moveToProfile: PropTypes.func.isRequired
};

const enhance = lifecycle({
  componentDidMount() {
    this.props.validateToken();
  }
});

const BaseContainer = enhance(({
  moveToSignup,
  moveToLogin,
  children,
  isLoading,
  authorized,
  error,
  createError,
  logout,
  loading,
  moveToProfile
}) => {
  if (isLoading || loading) {
    return (<Loading show />);
  }

  const actions = [
    <FlatButton
      label="Got it"
      primary
      keyboardFocused
      onTouchTap={() => createError('')}
    />,
  ];

  return (
    <div className="container">
      <NavButton
        authorized={authorized}
        moveToSignup={moveToSignup}
        moveToLogin={moveToLogin}
        logout={logout}
        moveToProfile={moveToProfile}
      />
      { error !== '' &&
        <Dialog
          title="Message"
          actions={actions}
          modal={false}
          open={error !== ''}
          onRequestClose={() => createError('')}
        >
          {error}
        </Dialog>
      }
      {children}
    </div>
  );
});


BaseContainer.propTypes = propTypes;

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    ...ProfileActions,
    ...UiActions,
  }, dispatch);
}

function mapStateToProps({ ui, profile }) {
  return {
    isLoading: profile.isLoading,
    authorized: window.localStorage.getItem('jwt_testapp') ? true : false,
    error: ui.get('error'),
    loading: ui.get('loading')
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BaseContainer);
