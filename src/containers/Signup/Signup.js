import React, { Component, PropTypes } from 'react';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import {
  createValidator,
  composeValidators,
  combineValidators,
  isRequired
} from 'revalidate';
import FlatButton from 'material-ui/FlatButton';

import { push } from 'react-router-redux';

import * as actionTypes from '../../actions/actionTypes';
import { createError, spinner } from '../../actions/ui';
import { urls } from '../../routes';

const isValidEmail = createValidator(
  message => (value) => {
    if (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
      return message;
    }
    return null;
  },
  'Invalid email address'
);

const validate = combineValidators({
  email: composeValidators(
    isRequired({ message: 'Required' }),
    isValidEmail
  )(),
  password: isRequired({ message: 'Required' }),
  confirmPassword: isRequired({ message: 'Required' })
});


/* eslint-disable react/prop-types */
const renderField = ({ input, label, type, name, meta: { touched, error } }) => (
  <div className={`form-group ${(!touched || error === undefined) ? '' : 'has-error'}`}>
    {touched && error
      ? <label htmlFor={`${name}-id`} className="error">{error}</label>
      : <label htmlFor={`${name}-id`}>{label}</label>
    }
    <input
      id={`${name}-id`}
      className="form-control"
      placeholder={label}
      type={type}
      {...input}
    />
  </div>
);
/* eslint-enable react/prop-types */

const propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  pristine: PropTypes.bool.isRequired
};


class Signup extends Component {
  /* eslint-disable no-unused-vars */
  static handleSubmit(value, dispatch) {
  /* eslint-enable no-unused-vars */
    let token = '';
    if (value.password !== value.confirmPassword) {
      dispatch(createError("Passwords doesn't match"));
      return;
    }
    dispatch(spinner(true));
    fetch('/api/v1/signup',
      { method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        // in the real life we should encrypt email and password
        // but this is just a sandbox app
        body: JSON.stringify(value)
      })
      .then((response) => {
        if (response.headers.get('Authorization')) {
          token = response.headers.get('Authorization').split(' ')[1];
        }
        return response.json();
      })
      .then((json) => {
        if (json.status === 'success') {
          window.localStorage.setItem('jwt_testapp', token);
          dispatch({ type: actionTypes.SET_PROFILE_ERROR, payload: false });
          dispatch(push(urls.fillProfile));
          dispatch(spinner(false));
          return;
        }
        dispatch(spinner(false));
        dispatch(createError(json.error));
      });
  }

  render() {
    const { handleSubmit, pristine, submitting, reset } = this.props;

    return (
      <div className="signupForm">
        <div className="col-md-3" />
        <h1>Signup</h1>
        <div className="col-md-6 col-offset-3">
          <form onSubmit={handleSubmit(Signup.handleSubmit)}>
            <Field
              name="email"
              type="email"
              label="Email address"
              component={renderField}
              placeholder="Email"
            />
            <Field
              name="password"
              type="password"
              label="Password"
              component={renderField}
              placeholder="Password"
            />
            <Field
              name="confirmPassword"
              type="password"
              label="Confirm password"
              component={renderField}
              placeholder="Confirm password"
            />
            <div className="buttons">
              <FlatButton type="submit" label="Submit" disabled={submitting} />
              <FlatButton
                type="button"
                label="Clear values"
                disabled={pristine || submitting}
                onClick={reset}
              />
            </div>
          </form>
        </div>
        <div className="linkedIn">
          <span>Via</span><a href="http://127.0.0.1:8080/oauth/linkedin_login">Linked<i>in</i></a>
        </div>
      </div>
    );
  }
}

Signup.propTypes = propTypes;

export default reduxForm({
  form: 'signupForm',
  validate
})(Signup);
