import React, { PropTypes } from 'react';

const propTypes = {
  message: PropTypes.string.isRequired
};

const ErrorMessage = ({ message }) => (
  <div>
    { message !== '' &&
      <div className="errorMessages">
        <p>{message}</p>
      </div>
    }
  </div>
);

ErrorMessage.propTypes = propTypes;

export default ErrorMessage;
