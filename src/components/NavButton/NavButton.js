import React, { PropTypes, Component } from 'react';
import Menu from 'material-ui/Menu';
import Paper from 'material-ui/Paper';
import MenuItem from 'material-ui/MenuItem';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();


const propTypes = {
  authorized: PropTypes.bool,
  moveToSignup: PropTypes.func.isRequired,
  moveToLogin: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  moveToProfile: PropTypes.func.isRequired
};

const style = {
  display: 'inline-block',
  margin: '16px 32px 16px 0',
};

class NavButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };
  }

  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  render() {
    const { authorized, moveToLogin, moveToSignup, moveToProfile, logout } = this.props;
    return (
      <div className="mainMenu">
        <Paper className="paper" style={style}>
          { !authorized ?
            <Menu
              autoWidth={false}
            >
              <MenuItem
                primaryText="Login"
                onTouchTap={() => this.handleRequestClose && moveToLogin()}
              />
              <MenuItem
                primaryText="Signup"
                onTouchTap={() => this.handleRequestClose && moveToSignup()}
              />
            </Menu> :
            <Menu>
              <MenuItem
                primaryText="Log out"
                onTouchTap={() => logout()}
              />
              <MenuItem
                primaryText="Profile"
                onTouchTap={() => moveToProfile()}
              />
            </Menu>
          }
        </Paper>
      </div>
    );
  }
}

NavButton.propTypes = propTypes;

export default NavButton;
