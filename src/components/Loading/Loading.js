import React, { PropTypes } from 'react';


const propTypes = {
  show: PropTypes.bool.isRequired
};


const Loading = ({ show }) => {
  if (!show) {
    return null;
  }
  return (
    <div className="spinner-wrap">
      <div className="loading">
        <div className="loader-walk">
          <div />
          <div />
          <div />
          <div />
          <div />
        </div>
      </div>
    </div>
  );
};

Loading.propTypes = propTypes;

export default Loading;
