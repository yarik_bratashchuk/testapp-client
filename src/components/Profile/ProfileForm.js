import React, { Component, PropTypes } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import FlatButton from 'material-ui/FlatButton';
import Autocomplete from 'react-google-autocomplete';


/* eslint-disable react/prop-types */
const Field = ({ label, type, name, value, onChange, disabled }) => (
  <div className="form-group">
    <label htmlFor={`${name}-id`}>{label}</label>
    { name === 'address' ?
      <Autocomplete
        id={`${name}-id`}
        className="form-google"
        type={type}
        placeholder={label}
        disabled={disabled}
        value={value}
        onChange={onChange}
        onBlur={onChange}
      /> :
      <input
        id={`${name}-id`}
        className="form-control"
        placeholder={label}
        disabled={disabled}
        type={type}
        value={value}
        onChange={onChange}
      />
    }
  </div>
);
/* eslint-enable react/prop-types */

const propTypes = {
  updateProfile: PropTypes.func.isRequired,
  user: ImmutablePropTypes.map.isRequired,
  updateField: PropTypes.func.isRequired
};


class ProfileForm extends Component {
  constructor(props) {
    super(props);
    this.enableEditing = this.enableEditing.bind(this);
    this.state = {
      disabled: true,
      submiting: false
    };
  }

  enableEditing() {
    this.setState({
      disabled: false
    });
  }

  updateProfile = (e) => {
    e.preventDefault();
    this.setState({
      submiting: true
    });
    this.props.updateProfile(() =>
      this.setState({
        submiting: false,
        disabled: true
      })
    );
  }

  render() {
    const { user, updateField } = this.props;
    const { disabled, submiting } = this.state;

    return (
      <div className="signupForm profile">
        <div className="col-md-3" />
        <h1>Profile</h1>
        <div className="col-md-6 col-offset-3">
          <form onSubmit={this.updateProfile}>
            <Field
              name="email"
              type="email"
              label="Email address"
              disabled={disabled}
              value={user.get('email')}
              onChange={e => updateField('email', e.target.value)}
              placeholder="Email"
            />
            <Field
              name="fullName"
              type="text"
              label="Full name"
              disabled={disabled}
              value={user.get('fullName')}
              onChange={e => updateField('fullName', e.target.value)}
              placeholder="Full name"
            />
            <Field
              name="address"
              type="text"
              label="Address"
              disabled={disabled}
              value={user.get('address')}
              onChange={e => updateField('address', e.target.value)}
              placeholder="Address"
            />
            <Field
              name="telephone"
              type="telephone"
              label="Telephone"
              disabled={disabled}
              value={user.get('telephone')}
              onChange={e => updateField('telephone', e.target.value)}
              placeholder="Telephone"
            />
            <div className="buttons">
              <FlatButton type="submit" label="Submit" disabled={disabled} />
              <FlatButton
                type="button"
                label="Edit"
                disabled={submiting}
                onClick={this.enableEditing}
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

ProfileForm.propTypes = propTypes;

export default ProfileForm;
