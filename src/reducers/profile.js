import { Record, Map, fromJS } from 'immutable';

import * as actionTypes from '../actions/actionTypes';

/* eslint-disable new-cap */
const InitialState = Record({
  isLoading: true,
  profileError: true,
  userData: Map()
});
/* eslint-enable new-cap */
const initialState = new InitialState();


export default function profile(state = initialState, action) {
  switch (action.type) {
    case actionTypes.CHANGE_USER_STATE:
      return state.set('isLoading', action.payload);
    case actionTypes.SET_PROFILE_ERROR:
      return state.set('profileError', action.payload);
    case actionTypes.FILL_PROFILE:
      return state.set('userData', fromJS(action.payload));
    case actionTypes.UPDATE_FIELD:
      return state.setIn(['userData', action.payload.field], action.payload.value);
    case actionTypes.LOG_OUT:
      return state.set('userData', new Map()).set('profileError', true);
    default:
      return state;
  }
}
