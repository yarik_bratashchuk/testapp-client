import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import profile from './profile';
import ui from './ui';


const rootReducer = combineReducers({
  routing: routerReducer,
  profile,
  form: formReducer,
  ui,
});

export default rootReducer;
