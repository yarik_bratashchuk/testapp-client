import { fromJS } from 'immutable';

import * as types from '../actions/actionTypes';

/* eslint-disable new-cap */
const initialState = fromJS({
  error: '',
  loading: false
});

export default function ui(state = initialState, action) {
  switch (action.type) {
    case types.SUBMIT_ERROR:
      return state.set('error', action.payload);
    case types.SPINNER:
      return state.set('loading', action.payload);
    default:
      return state;
  }
}

