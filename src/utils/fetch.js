import fetchMock from 'fetch-mock';
import moment from 'moment';

import config from '../config';
import { generateFakeBarberShop, generateFakeOrders } from '../utils/helpers';

function createFakeResponses() {
  if (config.fakeFetch) {
    fetchMock.restore();
    fetchMock.reset();
    fetchMock.mock(
      `${config.baseUrl}/barbershop`,
      new Promise(res => setTimeout(res, config.fakeDelay)).then(() => generateFakeBarberShop())
    );
    fetchMock.mock(
      `${config.baseUrl}/orders/${moment().format().substring(0, moment().format().indexOf('T'))}`,
      new Promise(res => setTimeout(res, config.fakeDelay)).then(() => generateFakeOrders())
    );
    fetchMock.mock(
      `${config.baseUrl}/updateOrder`,
      new Promise(res => setTimeout(res, config.fakeDelay)).then(() => {})
    );
  }
}

function getHeaders(checkJWT) {
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  };

  if (checkJWT) {
    const jwt = window.localStorage.jwt;
    if (jwt) {
      headers.Authorization = `JWT ${jwt}`;
    }
  }

  return headers;
}

export default function myFetch(url, meta = { method: 'GET' }, auth = true) {
  const metaData = { ...meta };
  if (metaData.headers === undefined) {
    metaData.headers = getHeaders(auth);
  }
  createFakeResponses();
  return fetch(url, metaData)
    .then(response => (response.json()));
}
