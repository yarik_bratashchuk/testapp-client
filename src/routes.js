import React from 'react';
import { Route, IndexRoute } from 'react-router';

import BaseContainer from './containers/Base/Base';

import Index from './containers/Index/Index';

import Signup from './containers/Signup/Signup';
import Login from './containers/Login/Login';

import FillProfile from './containers/Profile/FillProfile';
import Profile from './containers/Profile/Profile';

export const urls = {
  index: '',
  signup: '/signup',
  login: '/login',

  fillProfile: '/fillProfile',
  profile: '/Profile'

};

export const routes = (
  <Route path="/" component={BaseContainer}>
    <IndexRoute path={urls.index} component={Index} />
    <Route path={urls.signup} component={Signup} />
    <Route path={urls.login} component={Login} />

    <Route path={urls.fillProfile} component={FillProfile} />
    <Route path={urls.profile} component={Profile} />
  </Route>
);

