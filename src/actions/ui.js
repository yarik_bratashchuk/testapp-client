import * as actionTypes from './actionTypes';

export const createError = string => ({
  type: actionTypes.SUBMIT_ERROR,
  payload: string
});

export const spinner = show => ({
  type: actionTypes.SPINNER,
  payload: show
});
