// profile
export const CHANGE_USER_STATE = 'profile/CHANGE_USER_STATE';
export const SET_PROFILE_ERROR = 'profile/SET_PROFILE_ERROR';
export const FILL_PROFILE = 'profile/FILL_PROFILE';
export const LOG_OUT = 'profile/LOG_OUT';
export const UPDATE_FIELD = 'profile/UPDATE_FIELD';

// ui
export const ERROR_MESSAGE = 'ui/ERROR_MESSAGE';
export const SUBMIT_ERROR = 'ui/SUBMIT_ERROR';
export const SPINNER = 'ui/SPINNER';

