import fetch from 'isomorphic-fetch';
import { push } from 'react-router-redux';

import * as actionTypes from './actionTypes';
import { urls } from '../routes';
import { createError } from './ui';

export const moveToSignup = () => dispatch => dispatch(push(urls.signup));
export const moveToProfile = () => dispatch => dispatch(push(urls.profile));
export const moveToLogin = () => dispatch => dispatch(push(urls.login));


/* eslint-disable no-param-reassign */
function makeAuthenticatedRequest(url, opts = {}) {
  let token = window.location.search;
  if (token.length > 0) {
    token = token.split('=')[1];
    window.localStorage.setItem('jwt_testapp', token);
  } else {
    token = window.localStorage.getItem('jwt_testapp');
  }
  opts.method = 'GET';
  opts.headers = opts.headers || {};
  opts.headers.Authorization = `Bearer ${token}`;
  opts.headers['Content-Type'] = 'application/json';
  return fetch(url, opts);
}


export const validateToken = () => (dispatch) => {
  dispatch({ type: actionTypes.CHANGE_USER_STATE, payload: true });

  makeAuthenticatedRequest('/api/v1/verifyToken')
  .then(response => response.json()).then((json) => {
    if (json.status !== 'success') {
      window.localStorage.removeItem('jwt_testapp');
      dispatch(push(urls.index));
    }
    dispatch({ type: actionTypes.SET_PROFILE_ERROR, payload: false });
    dispatch({ type: actionTypes.CHANGE_USER_STATE, payload: false });
  });
};


export const signup = value => dispatch =>
  fetch('/api/v1/signup',
    { method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(value)
    })
    .then(response =>
      (response.json())
    )
    .then((json) => {
      if (json.status === 'success') {
        window.localStorage.setItem('jwt_testapp', json.token);
        dispatch({ type: actionTypes.SET_PROFILE_ERROR, payload: false });
        dispatch(push(urls.fillProfile));
        return;
      }
      dispatch({ type: actionTypes.ERROR_MESSAGE, payload: json.error });
    });


export const logout = () => (dispatch) => {
  window.localStorage.removeItem('jwt_testapp');
  dispatch({
    type: actionTypes.LOG_OUT
  });
  dispatch(push(urls.index));
};


export const loadProfile = () => (dispatch) => {
  fetch('/api/v1/my/profile',
    { method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${window.localStorage.getItem('jwt_testapp')}`,
        'Content-Type': 'application/json'
      }
    })
    .then(response => response.json())
    .then((json) => {
      if (json.status === 'success') {
        dispatch({
          type: actionTypes.FILL_PROFILE,
          payload: json.user
        });
        return;
      }
      dispatch(createError(json.error));
    });
};

export const updateProfile = cb => (dispatch, getState) => {
  const value = getState().profile.get('userData').toJS();
  if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value.email)) {
    dispatch(createError('Email is not valid'));
    return;
  }
  fetch('/api/v1/my/profile', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${window.localStorage.getItem('jwt_testapp')}`,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(value)
  })
  .then(response =>
    (response.json())
  )
  .then((json) => {
    if (json.status === 'success') {
      dispatch(createError('Done!'));
      cb();
      return;
    }
    dispatch(createError(json.error));
    cb();
  });
};

export const updateProfileField = (field, value) => ({
  type: actionTypes.UPDATE_FIELD,
  payload: {
    field, value
  }
});
